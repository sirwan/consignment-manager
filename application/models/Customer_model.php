<?php

class Customer_model extends CI_Model {

        public $customerid;
        public $ip_address;
        public $date_registered;
        public $fullname;
        public $dateofbirth;

        public $doornumber;
        public $address;
        public $postcode

        public $telephone;
        public $mobile;
        public $email;

        public $last_modified;
        public $last_updated_proof;

        public function __construct()
        {
                parent::__construct();
                $this->load->helper('date');
                $this->load->helper('url');
                $this->load->database();
        }

        public function get_all_customers(){
                //$query = $this->db->get('customers', 50);
                //return $query->result();
                $query = $this->db->get('customers');
                return $query->result_array();
        }

        public function get_customer(){
                $query = $this->db->get_where('news', array('slug' => $slug));
                return $query->row_array();
        }

        public function insert_customer(){

                $data = array(
                        'fullname' => $this->input->post('fullname'),
                        'telephone' => $this->input->post('telephone'),
                        'mobile' => $this->input->post('mobile'),
                        'email' => $this->input->post('email'),
                        'currentaddress' => $this->input->post('currentaddress'),
                        'currentpostcode' => $this->input->post('currentpostcode'),
                        'dateofbirth' => $this->input->post('dateofbirth'),
                        'customernotes' => $this->input->post('customernotes'),
                        'dateregistered' => time()
                    );

                return $this->db->insert('customers', $data);
        }

        public function update_customer()
        {
                // $this->title    = $_POST['title'];
                // $this->content  = $_POST['content'];
                // $this->date     = time();

                // $this->db->update('customers', $this, array('id' => $_POST['id']));
        }

}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

		if(!$this->ion_auth->logged_in()){
			redirect('auth/login', 'refresh');
		}
	}

	public function index(){
		$this->load->view('customers/index');
	}

	public function open_customer(){
		$this->load->view('customers/open_customer');
	}

	public function new_customer(){
		$this->load->view('customers/new_customer');
	}
}
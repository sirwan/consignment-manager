<?php
		$this->load->view('header');
?>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="page-header">
        <h1><i class="icon ion-person-add"></i> New Customer <small>Add new customers details.</small></h1>
      </div>

			<!-- <h1><i class="icon ion-person-add"></i> New Customer</h1>
			<p>Add the new customers information below.</p> -->
		</div>
	</div>

  <div class="row">
  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  		<!-- Form -->
      <?php //echo form_open('customers/new_customer'); ?>
      <form action="/customers/new_customer" method="post" accept-charset="utf-8" class="form-horizontal">
        <div class="form-group">
          <label for="fullName" class="col-sm-2 control-label">Full Name:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="fullName" placeholder="Full Name">
          </div>
        </div>

        <div class="form-group">
          <label for="telephone" class="col-sm-2 control-label">Telephone:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="telephone" placeholder="Telephone">
          </div>
        </div>

        <div class="form-group">
          <label for="mobile" class="col-sm-2 control-label">Mobile:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="mobile" placeholder="Mobile">
          </div>
        </div>
        
        <div class="form-group">
          <label for="email" class="col-sm-2 control-label">Email:</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" id="email" placeholder="Email">
          </div>
        </div>

        <div class="form-group">
          <label for="currentAddress" class="col-sm-2 control-label">Current Address:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="currentAddress" placeholder="e.g. Door Number, Road Name, Town, City, County">
          </div>
        </div>

        <div class="form-group">
          <label for="currentPostcode" class="col-sm-2 control-label">Current Postcode:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="currentPostcode" placeholder="Current Postcode">
          </div>
        </div>

        <div class="form-group">
          <label for="dateOfBirth" class="col-sm-2 control-label">Date of Birth</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="dateOfBirth" placeholder="DD-MM-YYYY">
            <span id="helpBlock" class="help-block">You must enter the date of birth in this format: <b>DD-MM-YYYY</b>. e.g. 06-05-1987</span>
          </div>
        </div>

        <div class="form-group">
          <label for="customerNotes" class="col-sm-2 control-label">Customer Notes</label>
          <div class="col-sm-10">
            <textarea id="customerNotes" name="customerNotes" class="form-control" rows="3" placeholder="Customer Notes"></textarea>
          </div>
        </div>

        <div class="form-group">
          <label for="pictureUpload" class="col-sm-2 control-label">Picture Upload</label>
          <div class="col-sm-10">
            <input type="file" id="pictureUpload" name="pictureUpload">
            <p class="help-block">Select customer picture.</p>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-2 control-label"></div>
          <div class="col-sm-10">
            <button type="submit" class="btn btn-default">Save</button>
          </div>
        </div>

      </form>
  	</div>	
  </div>

<?php $this->load->view('footer'); ?>
<?php
		$this->load->view('header');
?>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="page-header">
        <h1><i class="icon ion-ios-people"></i> All Customers <small>View all customer records.</small></h1>
      </div>
			<!-- <h1><i class="icon ion-ios-people"></i> Customer Database</h1>
			<p>Below is a list of customers.</p> -->
		</div>
	</div>

  <div class="row">
  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  		<table class="table table-hover" id="allcustomers">
  			<thead>
  				<tr>
  					<th>Full Name</th>
  					<th>Telephone</th>
  					<th>Last Modifed</th>
  					<th>Last Updated Proof</th>
  					<th>Customer Details</th>
  				</tr>
  			</thead>
  			<tbody>
  				<tr>
  					<td>Sirwan Qutbi</td>
  					<td>02088035631</td>
  					<td>23/4/15</td>
  					<td>2 months ago</td>
  					<td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
  				</tr>
          <tr>
            <td>Gregory Peck</td>
            <td>02084995622</td>
            <td>2/6/15</td>
            <td>1 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Fabio Maurizio</td>
            <td>02083334221</td>
            <td>23/4/15</td>
            <td>2 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Mohammed Khalif</td>
            <td>02088596987</td>
            <td>23/4/15</td>
            <td>2 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Shanequa Curry</td>
            <td>02088749888</td>
            <td>23/4/15</td>
            <td>2 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Hussein Faham</td>
            <td>02088035631</td>
            <td>23/4/15</td>
            <td>2 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Andrew Holmes</td>
            <td>02084995622</td>
            <td>2/6/15</td>
            <td>1 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Mathew Mclure</td>
            <td>02083334221</td>
            <td>23/4/15</td>
            <td>2 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Sarah Beany</td>
            <td>02088596987</td>
            <td>23/4/15</td>
            <td>2 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Shawn Ball</td>
            <td>02088749888</td>
            <td>23/4/15</td>
            <td>2 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Matazs Vitalz</td>
            <td>02088035631</td>
            <td>23/4/15</td>
            <td>2 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Gregz Gorgoan</td>
            <td>02084995622</td>
            <td>2/6/15</td>
            <td>1 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Tony Dell</td>
            <td>02083334221</td>
            <td>23/4/15</td>
            <td>2 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Mellissa Manconi</td>
            <td>02088596987</td>
            <td>23/4/15</td>
            <td>2 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
          <tr>
            <td>Nick Tightenbottom</td>
            <td>02088749888</td>
            <td>23/4/15</td>
            <td>2 months ago</td>
            <td><a href="<?php echo site_url('customers/open_customer'); ?>">Open</a></td>
          </tr>
  			</tbody>
  		</table>
  	</div>	
  </div>

<?php $this->load->view('footer'); ?>
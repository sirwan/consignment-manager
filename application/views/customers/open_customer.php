<?php
		$this->load->view('header');
?>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h1><i class="icon ion-person"></i> Edit Customer</h1>
			<p>Below is a list of customers.</p>
		</div>
	</div>

  <div class="row">
  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  		<table class="table table-hover">
  			<thead>
  				<tr>
  					<th>Full Name</th>
  					<th>Telephone</th>
  					<th>Last Modifed</th>
  					<th>Last Updated Proof</th>
  					<th>Customer Details</th>
  				</tr>
  			</thead>
  			<tbody>
  				<tr>
  					<td>Sirwan Qutbi</td>
  					<td>02088035631</td>
  					<td>23/4/15</td>
  					<td>2 months ago</td>
  					<td><a href="#">View</a></td>
  				</tr>
  			</tbody>
  		</table>
  	</div>	
  </div>

<?php $this->load->view('footer'); ?>
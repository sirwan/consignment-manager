<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $this->config->item('site_name'); ?> - <?php echo $this->config->item('site_description'); ?></title>

    <!-- jQuery 2.1.4 -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo site_url('assets/css/bootstrap-cosmo.min.css'); ?>" rel="stylesheet">
    <script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>

    <!-- Custom CSS -->
    <link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo site_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/bs/dt-1.10.10,cr-1.3.0,r-2.0.0/datatables.min.css"/> 
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10,cr-1.3.0,r-2.0.0/datatables.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">

      $(document).ready(function() {
        $('#allcustomers').DataTable();
      } );

    </script>

  </head>
  <body>

  <?php 
    $uri =  $this->router->fetch_class();
    $uri = $this->uri->segment(1);
  
    $currentlyOnCustomers="";
    $currentlyOnItems ="";
    $currentlyOnHistory="";
    $currentlyOnPermissions ="";

    if ($uri=="customers"){
      $currentlyOnCustomers = 'active';
    }elseif ($uri=="items") {
      # code...
    }elseif ($uri=="history") {
      # code...
    }elseif ($uri=="auth") {
      # code...
      $currentlyOnPermissions = 'active';
    }
  ?>
  <?php if ($this->ion_auth->logged_in()) { ?>



<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="<?php echo site_url('assets/images'); ?>/made-by-produce-code-white.png"></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">
        

        <li class="dropdown <?php echo "$currentlyOnCustomers"; ?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="icon ion-ios-people"></i> Customers <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<?php echo site_url('customers');?>"><i class="icon ion-person-stalker"></i> All Customers</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo site_url('customers/new_customer'); ?>"><i class="icon ion-person-add"></i> New Customer</a></li>
          </ul>
        </li>
        <li class="<?php echo "$currentlyOnItems"; ?>"><a href="#"><i class="icon ion-pricetag"></i> Items</a></li>
        <li class="<?php echo "$currentlyOnHistory"; ?>"><a href="#"><i class="icon ion-clipboard"></i> History</a></li>
      </ul>



      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown <?php echo "$currentlyOnPermissions"; ?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="icon ion-android-unlock"></i> Permissions <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('auth');?>">All Users</a></li>
            <li><a href="<?php echo site_url('auth/create_user'); ?>">Create User</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo site_url('auth/create_group'); ?>">Create Group</a></li>
          </ul>
        </li>
        <li><a href="<?php echo site_url('auth/logout'); ?>"><i class="icon ion-log-out"></i> Sign Out</a></li>
      </ul>


    </div>
  </div>
</nav>

  <?php } else { ?>    

        <nav class="navbar navbar-default navbar-inverse navbar-static-top">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo site_url(); ?>"><?php echo $this->config->item('site_name');?></a>
            </div>
            <p class="navbar-text"><?php echo $this->config->item('site_description'); ?></p>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                
              </ul>

            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

  <?php } ?>

  <div class="container">

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <a href="<?php echo site_url(); ?>">
        <img src="<?php echo site_url('assets/images'); ?>/consignment-manager-logo.png">
      </a>
    </div>

  </div>